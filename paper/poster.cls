\LoadClass[a4paper,landscape,oneside]{memoir}

\RequirePackage{geometry}
\RequirePackage{fontspec}
\RequirePackage{graphicx}
\RequirePackage{tikz}

\def \logopath{../graphics/aucs_logo.png}
\def \fontpath{../graphics/font/xolonium/Xolonium-Regular.otf}

% TODO: Make this use the \fontpath attribute.
\setmainfont{Xolonium-Regular.otf}[
    Path = ../graphics/font/xolonium/
]

% Set spacing on our elements.
\setlength{\parskip}{0.7cm}

\newgeometry{vmargin={10mm}, hmargin={50mm}}
\centering
\pagenumbering{gobble}